package Spring;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import javax.swing.*;

public class LieuxDemonstrator 
{
	ArrayList<Integer> dataVillage = new ArrayList<Integer>(), dataPlace = new ArrayList<Integer>();
	ArrayList<String> villages = new ArrayList<String>(), places = new ArrayList<String>();
	boolean contin = true;
	int lengthOfMonth[] = new int[] {31,28,31,30,31,30,31,31,30,31,30,31};
	
	//Parameters to change
	final int timeMs = 250;
	final boolean showVillages = false;
	final boolean showInLog = true;
	
	public LieuxDemonstrator()
	{
		if(showVillages)
			demonstrate(dataVillage, villages);
		else
			demonstrate(dataPlace, places);
	}
	
	void demonstrate(ArrayList<Integer> data, ArrayList<String> names)
	{
		loadData("/home/blasebalken/Desktop/Lieux");
		int size = dataVillage.size();
		System.out.println(size);
		int maxx = 0, currentCount = 1, highestPlace = 0;
		for(int i=1;i<size;i++)
		{
			if(dataVillage.get(i-1)==dataVillage.get(i))
				currentCount++;
			else
				currentCount = 0;
			if(maxx<currentCount)
			{
				maxx = currentCount;
				highestPlace = dataVillage.get(i);
			}
		}
		System.out.println(villages.get(highestPlace)+ " " + maxx);
		/*System.out.println("");
		for(String s : places)
			System.out.print(s+" ");
		System.out.println("");
		for(int i : dataPlace)
			System.out.print(i+" ");
		System.out.println("");
		for(int i : dataPlace)
			System.out.print(places.get(i)+" ");
		System.out.println("Finished");*/
		JFrame f = new JFrame();
		ArrayList<Integer> currentData = new ArrayList<Integer>();
		int max = 0;
		for(int x : data)
			if(x>max)
				max = x;
		final int highest = max;
		class panel extends JPanel
		{
			@Override
			public void paintComponent(Graphics g)
			{
				this.removeAll();
				g.setColor(Color.black);
				g.fillRect(0, 0, 1000, 1000);
				int size = currentData.size();
				if(size>0)
				{
					int length = (int)Math.floor(900/(size));
					int dayOfYear = lengthOfMonth[0];
					int month = 1;
					g.setColor(Color.white);
					while(dayOfYear<=size)
					{
						g.drawLine(dayOfYear*length, 1000, dayOfYear*length, 0);
						dayOfYear += lengthOfMonth[month];
						month++;
					}
					int counting[] = new int[highest+1];
					Color colors[] = new Color[highest+1]; 
					for(int i=0;i<highest+1;i++)
				        colors[i] = Color.getHSBColor((float) i / (float) (highest+1), 0.85f, 1.0f);
					int max = 0;
					for(int x : currentData)
						counting[x]++;
					for(int x : counting)
						if(x>max)
							max = x; 	
					for(int x=0;x<=highest;x++)
						counting[x] = 0;
					int height = (int)Math.floor(1000/max);
					for(int x=0;x<size;x++)
						{
						int current = currentData.get(x);
						for(int y=0;y<=highest;y++)
						{
							g.setColor(colors[y]);
							if(showInLog)
							{
								if(y!=current)
									g.drawLine(x*length, 1000-(int)Math.floor(1000*Math.log(counting[y]+1)/Math.log(max+1)), (x+1)*length, 1000-(int)Math.floor(1000*Math.log(counting[y]+1)/Math.log(max+1)));
								else
									g.drawLine(x*length, 1000-(int)Math.floor(1000*Math.log(counting[y]+1)/Math.log(max+1)), (x+1)*length, 1000-(int)Math.floor(1000*Math.log(counting[y]+1+1)/Math.log(max+1)));
							}
							else
							{
								if(y!=current)
									g.drawLine(x*length, 1000-counting[y]*height, (x+1)*length, 1000-counting[y]*height);
								else
									g.drawLine(x*length, 1000-counting[y]*height, (x+1)*length, 1000-(counting[y]+1)*height);
							}
							//System.out.println((x*length) + " - " + (1000-counting[y]*height)  + " - " +  ((x+1)*length) + " - " +  (1000-counting[y]*height));
						}
						//System.out.println("");
						counting[current]++;	
						if(x==size-1)
							for(int i=0;i<=highest;i++)
								if(counting[i]>0)
								{
									String s = "";
									for(int j=0;j<=highest;j++)
										if(counting[j]==counting[i])
											s += "<html><font color=\'"+"#"+Integer.toHexString(colors[j].getRGB()).substring(2)+"\' size=\"3\">"+names.get(j)+"</font>\n";
									JLabel l = new JLabel(s + "<html><font color=white size=\"3\">" + counting[i]+"</font></html>");
									if(showInLog)
										l.setBounds(900, 1000-(int)Math.floor(1000*Math.log(counting[i]+1)/Math.log(max+1))-146,100,300);
									else
										l.setBounds(900, 1000-counting[i]*height-146,100,300);
									this.add(l);
								}			
					}
				//System.out.println("----------------------");
				}
			}
		}
		JPanel p = new panel();
		f.setSize(1000,1000);
		f.setResizable(false);
		f.setLocationRelativeTo(null);
		f.setUndecorated(true);
		f.getContentPane().add(p);
		f.setVisible(true);
		f.repaint();
		Timer t;
		ActionListener action = new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
					try
					{
					currentData.add(data.get(currentData.size()));
					}
					catch(IndexOutOfBoundsException f)
					{contin = false;}
				if(contin)
				{
					p.repaint();
					f.getContentPane().repaint();
					f.repaint();
				}
			}
		};
		t = new Timer(timeMs, action);
		t.start();
	}
	
	public void loadData(String path)
	{
		try
		{
		FileInputStream fis = new FileInputStream(path);
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		String s;
		while((s=br.readLine())!=null)
		{
			boolean first = true;
			byte mode = 0;
			int start = 0;
			int size = s.length();
			for(int i=0;i<size;i++)
			{
				//System.out.print(s.charAt(i)+""+mode+"/ ");
				switch(mode)
				{
				case 0: if(i>0&&s.charAt(i)==' '&&s.charAt(i-1)==' ')
							mode = 1;
						break;
				case 1: if(s.charAt(i)!=' ')
						{
							start = i;
							mode = 2;
						}
						if(s.charAt(i)!='-')
							break;
				case 2: if(i==size-1||(s.charAt(i)==' '&&s.charAt(i+1)==' '))
						{
							int end = 0;
							if(i==size-1)
								end = i+1;
							else 
								end = i;
							String word = s.substring(start, end);
							if(s.charAt(start)=='-')
								if(first)
									word = villages.get(dataVillage.get(dataVillage.size()-1));
								else
									word = places.get(dataPlace.get(dataPlace.size()-1));
							int toAdd = search(word, first);
							if(first)
								dataVillage.add(toAdd);
							else
								dataPlace.add(toAdd);
							first = false;
							mode = 0;
						}
						break;
				}
			}
			//System.out.println("");
		}
		}
		catch(IOException e)
		{e.printStackTrace();}
	}
	
	public int search(String s, boolean which)
	{
		//System.out.print(s);
		ArrayList<String> strings;
		if(which)
			strings = villages;
		else
			strings = places;
		int size = strings.size();
		int position = size;
		for(int i=0;i<size;i++)
			if(s.equals(strings.get(i)))
			{
				position = i;
				break;
			}
		if(position==size)
			if(which)
				villages.add(s);
			else
				places.add(s);
		//System.out.print(position+" ");
		return position;
	}
	
	public static void main(String[] args)
	{
		LieuxDemonstrator lieux = new LieuxDemonstrator();
	}
}
