package Spring;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.stream.IntStream;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class ColoursFrame extends JFrame implements ActionListener
{
	Timer t;
	Color colors [][];
	private int sizeX, sizeY;
	Random rand;
	
	public ColoursFrame(int timeMilli, int sizeX, int sizeY)
	{
		this.sizeX = sizeX; this.sizeY = sizeY;
		rand = new Random();
		colors = new Color[sizeX][sizeY];
		for(int x=0;x<sizeX;x++)
			for(int y=0;y<sizeY;y++)
			{
				colors[x][y] = Color.white;
			}
		t = new Timer(timeMilli, this);
		JPanel p = new panel();
		this.setUndecorated(true);
		this.setSize(sizeX,sizeY);
		this.setLocationRelativeTo(null);
		this.getContentPane().add(p);
		this.setVisible(true);
		this.getContentPane().repaint();
		t.start();
	}

	public static void main(String[] args)
	{
		ColoursFrame cf = new ColoursFrame(1, 1000, 1000);
	}
	
	public class panel extends JPanel
	{
		public void paintComponent(Graphics g)
		{
			for(int x=0;x<sizeX;x++)
				for(int y=0;y<sizeY;y++)
				{
					g.setColor(colors[x][y]);
					g.fillRect(x, y, 1, 1);
				}
		}
	}

	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource().equals(t))
		{
			System.out.println("Ha");
			Color color = new Color(rand.nextInt(128),rand.nextInt(128),rand.nextInt(128));
			int x = rand.nextInt(sizeX);
			int y = rand.nextInt(sizeY);
			colors[x][y] = color;
			this.repaint();
		}
	}
}
