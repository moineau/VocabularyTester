package Spring;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import java.io.*;
import java.lang.reflect.InvocationTargetException;

public class VocabularyTester
{
	frame big, big2;
	JFrame small;			//JFrames which are the different Menus with changing panels
	String languagesPath = "/home/blasebalken/Desktop/languages";
	ArrayList<language> languages;
	
	record vocabLocation ( int word, byte language ) {}
	record stringPair ( String word1, String word2 ) {}
	
	//You need to write all the small menus(all open functions), make the flow to test() seamless and maybe make the code more beautiful?
	
	@SuppressWarnings("unchecked")
	public VocabularyTester()
	{
		big = new frame();
		big2 = new frame();
		big.setVisible(true);
		languages = (ArrayList<language>) readFile(languagesPath, ArrayList.class);
		//Start of test part simulating a language
		ArrayList<wordInstance> vocabs = new ArrayList<wordInstance>();
		multWords multi = new multWords("Schaff","peco");
		for(int i=0;i<4;i++)
			multi.add(new Word("Hallo", "Ciao", 0, 0, 0, 0));
		for(int i=0;i<25;i++)
			multi.add(new Word("Schafeeeeeeeeeeeeeeeeeeeee", "pecora", 0, 0, Long.MAX_VALUE, Long.MAX_VALUE));
		vocabs.add(new Word("Hallo", "Ciao", 0, 0, 0, 0));
		vocabs.add(new Word("Schafeeeeeeeeeeeeeeeeeeeee", "pecora", 0, 0, Long.MAX_VALUE, Long.MAX_VALUE));
		vocabs.add(multi);
		language lang = new language("German", "Italian", vocabs);
		//languages.set(0, lang);
		test(0);
		//vocabMenu(0);
		//choiceMenu(0);
		//End of test part
		langMenu();
	}
	
	class smallFrame extends JFrame
	{
		public smallFrame()
		{
			setSize(200,400);
			setLocationRelativeTo(null);
			setResizable(false);
			setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
			setVisible(true);
		}
	}
	
	class frame extends JFrame
	{
		Callable<Void> function;
		
		public frame()
		{
			setSize(1000,1000);
			setLocationRelativeTo(null);
			setResizable(false);
			setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
			addWindowListener(new WindowAdapter()	//Checks if you want to close window or go back
			{
				@Override
				public void windowClosing(WindowEvent e)
				{
					int exitMode = JOptionPane.showOptionDialog(null, "How do you want to exit this page?", "Exit Page", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[]{"Exit all", "Go Back", "Cancel"}, "Cancel");
					if(exitMode==JOptionPane.YES_OPTION)
						System.exit(0);
					else if(exitMode==JOptionPane.NO_OPTION)
						goBack();
				}
			});
		}
		
		public void setCallable(Callable<Void> newFunction)
		{
			function = newFunction;
		}
		
		public void goBack()		//Goes to menu before the current one, gets overwritten
		{
			try {
				function.call();
			} catch (Exception e) {}
		}
	}
	
	void langMenu()
	{
		big.getContentPane().removeAll();
		JPanel panel = new JPanel();
		JScrollPane sp = new JScrollPane(panel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		sp.getVerticalScrollBar().setUnitIncrement(16);		//Fast Scrollbar
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		int size = languages.size();
		c.gridy = 0;					
		c.ipady = 40;
		for(int i=0;i<size;i++)
		{
			c.gridy = 2*i;
			JLabel label = new langField(i);
			panel.add(label, c);
			c.gridy = 2*i+1;
			c.ipady = 40;
		}
		JButton addNew = new JButton("+");
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		addNew.setFont(new Font("Serif", Font.PLAIN, 40));
		addNew.setFocusPainted(false);
		addNew.setBackground(Color.white);
		addNew.addActionListener(e -> langAdd());
		addNew.setPreferredSize(new Dimension(70,70));
		buttonPanel.add(addNew);
		c.gridy = 2*size;
		panel.add(buttonPanel, c);
		c.gridy = 2*size+1;
		c.ipady = 40;
		big.add(sp);
		big.revalidate();
	}
	
	class langField extends JLabel
	{
		public langField(int lang)
		{
			this.setText(languages.get(lang).getName());
			this.setFont(new Font("Serif", Font.PLAIN, 20));
			this.addMouseListener(new MouseAdapter()		//Calls the word Menu when rightclicking on word
			{
				public void mousePressed(MouseEvent e)
				{
					if(e.getButton() == MouseEvent.BUTTON3)
							langOptions(lang, false);
					if(e.getButton() == MouseEvent.BUTTON1)
							choiceMenu(lang);
				}
			});
		}
	}
	
	
	void langOptions(int lang, boolean newLang)
	{
		JFrame frame = new smallFrame();
		frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.PAGE_AXIS));
		JPanel row0 = new JPanel(), row1 = new JPanel(), row2 = new JPanel(), row3 = new JPanel(), row4 = new JPanel();
		JButton delete = new JButton("Delete"), save = new JButton("Save"), exit = new JButton("Exit"), generate = new JButton();
		JLabel lang1 = new JLabel("Language 1:"), lang2 = new JLabel("Language 2:"), name = new JLabel("Name:");
		JTextField lang1Field = new JTextField(languages.get(lang).getLangName((byte)0)),
					lang2Field = new JTextField(languages.get(lang).getLangName((byte)1)),
					nameField = new JTextField(languages.get(lang).getName());
		lang1Field.setPreferredSize(new Dimension(100,18));
		lang2Field.setPreferredSize(new Dimension(100,18));
		nameField.setPreferredSize(new Dimension(120,18));
		generate.setPreferredSize(new Dimension(18,18));
		generate.setBackground(Color.black);
		delete.setBackground(Color.red);
		delete.setFocusPainted(false);
		generate.addActionListener(e -> nameField.setText(lang1Field.getText()+ " - "+lang2Field.getText()));
		delete.addActionListener(e -> 
		{languages.remove(lang);frame.dispose();writeFile(languagesPath, languages);langMenu();});
		exit.addActionListener(newLang ? e -> {languages.remove(lang); frame.dispose();}: e -> frame.dispose());
		save.addActionListener(e ->
		{languages.get(lang).LangName((byte)0, lang1Field.getText());
		 languages.get(lang).LangName((byte)1, lang2Field.getText());
		 languages.get(lang).setName(nameField.getText());
		 frame.dispose();writeFile(languagesPath, languages);langMenu();});
		if(!newLang)
			row0.add(delete);
		row1.add(lang1); row1.add(lang1Field);
		row2.add(lang2); row2.add(lang2Field);
		row3.add(name); row3.add(nameField); row3.add(generate);
		row4.add(save); row4.add(exit);
		frame.add(row0); frame.add(row1); frame.add(row2);
		frame.add(row3); frame.add(row4);
		frame.repaint();
		frame.revalidate();
	}
	
	void langAdd()
	{
		language newLanguage = new language("", "");
		languages.add(newLanguage);
		langOptions(languages.size()-1, true);
	}
	
	void choiceMenu(int lang)
	{
		big.getContentPane().removeAll();
		big.setCallable(new Callable<Void>() { 
			public Void call() {
				langMenu();
				return null;
			}
		});				//Implements where to go back to(what last menu was)
		JPanel panel = new JPanel();
		JButton doTest = new JButton("Test");
		JButton goMenu = new JButton("Vocabularies");
		doTest.setFont(new Font("Serif", Font.PLAIN, 60));
		goMenu.setFont(new Font("Serif", Font.PLAIN, 60));
		doTest.setFocusPainted(false);
		doTest.setBackground(Color.white);
		goMenu.setFocusPainted(false);
		goMenu.setBackground(Color.white);
		panel.setLayout(new FlowLayout());
		panel.add(Box.createRigidArea(new Dimension(1000, 250)));
		panel.add(doTest);
		panel.add(Box.createRigidArea(new Dimension(1000, 150)));
		panel.add(goMenu);
		panel.add(Box.createRigidArea(new Dimension(1000, 100)));
		//panel.setBackground(Color.darkGray);
		//doTest.addActionListener(e -> {System.out.println(panel.isVisible());big.remove(panel);big.revalidate();big.repaint();System.out.println(panel.isVisible());test(lang);});
		/*doTest.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent e) {
						System.out.println(panel.isShowing());
						for(Component comp : big.getComponents())
							System.out.println(comp);
						big.getContentPane().remove(panel);
						big.remove(panel);
						big.getContentPane().removeAll();
						big.removeAll();
						big.setSize(new Dimension(900,800));
						for(Component comp : big.getComponents())
							System.out.println(comp);
						big.revalidate();
						big.getContentPane().revalidate();
						big.getContentPane().repaint();
						big.repaint();
						System.out.println(panel.isShowing());
						test(lang);
					}
			
				});*/
		doTest.addActionListener(
				e -> 
				{big.remove(panel);
				big.revalidate();
				big.repaint();
				test(lang);
				});
		goMenu.addActionListener(e -> {
			big.remove(panel); 
		big.revalidate();
		big.repaint();
		vocabMenu(lang);
		}
		);
		big.add(panel);
		big.revalidate();
	}
	
	void test(int lang)
	{
		System.out.println("Hey");
		big.setCallable(new Callable<Void>() { 
			public Void call() {
				choiceMenu(lang);
				return null;
			}				//Implements where to go back to(what last menu was)
		});
		int length = languages.get(lang).getLang().size();	//Number of vocabs
		//Lists with the elements 0 to length-1 in random order
		ArrayList<Integer> referencesLang0 = scrambledListOfNumbers(length);
		ArrayList<Integer> referencesLang1 = scrambledListOfNumbers(length);
		//Goes through the words and checks if they are ready to be tested
		referencesLang0 = new ArrayList<>(referencesLang0.stream()
				.filter(references -> languages.get(lang).getLang().get(references).testable((byte)0))
				.collect(Collectors.toList()));
		referencesLang1 = new ArrayList<>(referencesLang1.stream()
				.filter(references -> languages.get(lang).getLang().get(references).testable((byte)1))
				.collect(Collectors.toList()));
		ArrayList<vocabLocation> testableVocabs = new ArrayList<vocabLocation>();
		//Gives the vocabs the refering language
		testableVocabs.addAll(referencesLang0.stream()
				.map(reference -> new vocabLocation(reference, (byte)0)).collect(Collectors.toList()));
		testableVocabs.addAll(referencesLang1.stream()
				.map(reference -> new vocabLocation(reference, (byte)1)).collect(Collectors.toList()));
		Collections.shuffle(testableVocabs);	//Creates a random list of all vocabs that can be tested
		firstVocabs(true, testableVocabs, lang);
	}
	
	ArrayList<Integer> scrambledListOfNumbers(int x)		//Creates list with the elements 0 to x-1 in random order
	{
		//Creates the list (0,1,2,3,4,...,x-1)
		ArrayList<Integer> counting = new ArrayList<Integer>(IntStream.range(0, x).boxed().collect(Collectors.toList()));
		Collections.shuffle(counting);
		return counting;
	}
	
	void firstVocabs(boolean save, ArrayList<vocabLocation> references, int lang)
	{
		int size = references.size();
		for(int i=size-1;i>=0;i--)
		{
			vocabLocation reference = references.get(i);
			wordInstance word = languages.get(lang).getLang().get(reference.word());
			if(word.isSingular())
			{
				Word singWord = (Word)word;
				CompletableFuture<Boolean> future = testFrameWord(singWord.getWord(reference.language()),singWord.getWord((byte)((reference.language()+1)%2)),references.size());
				try{
					if(future.get())
					{
						references.remove(i);
						if(save)
						{
							singWord.setLevel(singWord.getLevel(reference.language())+1, reference.language());	//Changes the data of the word
							singWord.setTime(System.currentTimeMillis(), reference.language());
							languages.get(lang).getLang().set(reference.word(), singWord);	//Than changes the data in the language
							writeFile(languagesPath, languages);		//Than saves it
						}
					}
					else if(save)
					{
						singWord.setLevel(Math.max(0,singWord.getLevel(reference.language())-1), reference.language());	//Changes the data of the word
						languages.get(lang).getLang().set(reference.word(), singWord);	//Than changes the data in the language
						writeFile(languagesPath, languages);		//Than saves it
					}
				}catch (InterruptedException | ExecutionException e) {e.printStackTrace();}
			}
			else
			{
				try {
					multWords multWord = testFrameMultWord((multWords)word,reference.language()).get();
					if(save)
					{
						languages.get(lang).getLang().set(reference.word(), multWord);
						writeFile(languagesPath, languages);
					}
					if(!multWord.testable(reference.language()))
						references.remove(i);
				} catch (InterruptedException | ExecutionException e) {e.printStackTrace();}
				
			}
		}
		if(!references.isEmpty())
		{
			Collections.shuffle(references);
			firstVocabs(false, references, lang); 		//Calls itself without changing data, until everything has been answered correctly
		}
	}
	
	CompletableFuture<Boolean> testFrameWord(String displayedWord, String hiddenWord, int wordsLeft)
	{
		big.getContentPane().removeAll();
		//TO DO add titleWords test
		JPanel panel = new JPanel();
		JLabel label = new JLabel(displayedWord);
		JTextField field = new JTextField("");
		boolean firstTime[] = new boolean[] {true};
		boolean correct[] = new boolean[] {false};
		CompletableFuture<Boolean> future = new CompletableFuture<>();
		KeyListener keyList = new KeyListener(){
			public void keyPressed(KeyEvent e) 	
			{
				if(e.getKeyCode()==10&&!firstTime[0])				//Goes to next word, so closes itself
				{
					big.remove(panel);
					big.repaint();
					future.complete(correct[0]);
				}
				if(e.getKeyCode()==10&&firstTime[0])			//Reacts if enter is pressed, so if the person has finished
				{
					firstTime[0] = false;
					correct[0] = hiddenWord.toLowerCase().equals(field.getText().toLowerCase());
					if(correct[0])					//True if correct word has been guessed.
						label.setText("Correct word! Only "+(wordsLeft-1)+" words are left.");
					else
						label.setText("Wrong word. Still "+wordsLeft+" words are left.");	//One more word because of the mistake
					field.setText(hiddenWord);
				}
			}
			public void keyTyped(KeyEvent e) {}
			public void keyReleased(KeyEvent e) {}
		};
		panel.addKeyListener(keyList);
		label.setFont(new Font("Serif", Font.PLAIN, 30));
		field.setFont(new Font("Serif", Font.PLAIN, 30));
		field.setPreferredSize(new Dimension(400,100));
		field.addKeyListener(keyList);
		panel.setLayout(new FlowLayout());
		panel.add(Box.createRigidArea(new Dimension(1000, 100)));
		panel.add(label);
		panel.add(Box.createRigidArea(new Dimension(1000, 100)));
		panel.add(field);
		panel.add(Box.createRigidArea(new Dimension(1000, 100)));
		big.add(panel);
		field.requestFocusInWindow();
		big.revalidate();
		return future;
	}
	
	CompletableFuture<multWords> testFrameMultWord(multWords words, byte b)
	{
		big.getContentPane().removeAll();
		int size = words.size();
		JPanel panel = new JPanel();
		JLabel results[] = new JLabel[size];
		JTextField field[] = new JTextField[size];
		JScrollPane sp = new JScrollPane(panel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		sp.getVerticalScrollBar().setUnitIncrement(16);		//Fast Scrollbar
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		CompletableFuture<multWords> future = new CompletableFuture<>();
		boolean firstTime[] = new boolean[]{true};
		KeyListener keyList = new KeyListener(){
			public void keyPressed(KeyEvent e) 	
			{
				if(e.getKeyCode()==10&&!firstTime[0])
				{
					big.remove(sp);
					big.repaint();
					future.complete(words);
				}
				if(e.getKeyCode()==10&&firstTime[0])
				{
					for(int i=0;i<size;i++)
						if(field[i]!=null)
						{
							Word currentWord = words.get(i);
							if(field[i].getText().toLowerCase().equals(currentWord.getWord((byte)((b+1)%2)).toLowerCase()))
							{
								results[i].setText("Right");
								currentWord.setLevel(currentWord.getLevel(b)+1, b);
								currentWord.setTime(System.currentTimeMillis(), b);
							}
							else
							{
								results[i].setText("Wrong");
								if(currentWord.getLevel(b)>0)
									currentWord.setLevel(currentWord.getLevel(b)-1, b);
							}
							words.set(i, currentWord);
							field[i].setText(words.get(i).getWord((byte)((b+1)%2)));
						}
					firstTime[0] = false;
					big.repaint();
				}
			}
			public void keyTyped(KeyEvent e) {}
			public void keyReleased(KeyEvent e) {}
		};
		sp.addKeyListener(keyList);
		//This is all the layout
		c.gridy = 0;					
		c.ipady = 40;
		int firstField = size;
		for(int i=0;i<size;i++)
		{
			c.gridy = i*2+2;
			JLabel l = new JLabel(words.get(i).getWord(b));
			l.setFont(new Font("Serif", Font.PLAIN, 20));
			c.gridx = 0;
			panel.add(l, c);
			c.gridx = 1;
			panel.add(Box.createRigidArea(new Dimension(50,0)), c);
			c.gridx = 2;
			if(words.get(i).testable(b))
			{
				field[i] = new JTextField();
				field[i].addKeyListener(keyList);
				field[i].setFont(new Font("Serif", Font.PLAIN, 20));
				if(firstField==size)
					firstField = i;
				JPanel pane = new JPanel(new GridBagLayout());
				pane.add(field[i]);
				field[i].setPreferredSize(new Dimension(200,40));
				panel.add(pane, c);
			}
			else
			{ 
				JLabel ll = new JLabel(words.get(i).getWord((byte)((b+1) % 2)));
				ll.setFont(new Font("Serif", Font.PLAIN, 20));
				panel.add(ll, c);
			}
			c.gridx = 3;
			panel.add(Box.createRigidArea(new Dimension(50,0)), c);
			c.gridx = 4;
			results[i] = new JLabel();
			results[i].setFont(new Font("Serif", Font.PLAIN, 20));
			panel.add(results[i], c);
			c.gridy = 2*i+3;
			c.ipady = 40;
		}
		c.gridy = 2*size;
		c.ipady = 40;
		big.getContentPane().add(sp);
		if(firstField<size)
			field[firstField].requestFocusInWindow(); 	//Focus first TextField
		big.revalidate();
		return future;
	}
	
	void vocabMenu(int lang)
	{
		big.setCallable(new Callable<Void>() { 
			public Void call() {
				choiceMenu(lang);
				return null;
			}				//Implements where to go back to(what last menu was)
		});	
		big.getContentPane().removeAll();
		JPanel panel = new JPanel();
		JScrollPane sp = new JScrollPane(panel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		sp.getVerticalScrollBar().setUnitIncrement(16);		//Fast Scrollbar
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		int size = languages.get(lang).getLang().size();
		c.gridy = 2*size+1;					
		c.ipady = 40;
		JTextField first = new JTextField();
		if(!languages.get(lang).getLang().isEmpty())
			first = new wordField(new vocabLocation(0,(byte)0), lang);	//Create it to focus it
		for(int i=0;i<size;i++)
		{
			c.gridy = 2*(size-i);
			JTextField field1 = new wordField(new vocabLocation(i,(byte)0), lang);
			if(i==0)
				field1 = first;
			JPanel pane1 = new JPanel(new GridBagLayout());
			pane1.add(field1);
			c.gridx = 0;
			panel.add(pane1, c);
			JTextField field2 = new wordField(new vocabLocation(i,(byte)1), lang);
			if(!languages.get(lang).getLang().get(i).isSingular())
			{
				field1.setBackground(Color.cyan);
				field2.setBackground(Color.cyan);
			}
			JPanel pane2 = new JPanel(new GridBagLayout());
			pane2.add(field2);
			c.gridx = 1;
			panel.add(pane2, c);
			c.gridy = 2*(size-i)+1;
			c.ipady = 40;
		}
		JButton addNew = new JButton("+");
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		addNew.setFont(new Font("Serif", Font.PLAIN, 40));
		addNew.setFocusPainted(false);
		addNew.setBackground(Color.white);
		addNew.addActionListener(e -> addWord(lang, -1));
		addNew.setPreferredSize(new Dimension(70,70));
		buttonPanel.add(addNew);
		c.gridy = 1;
		panel.add(buttonPanel, c);
		c.gridy = 0;
		c.ipady = 40;
		big.add(sp);
		first.requestFocusInWindow();
		big.revalidate();
	}
	
	class wordField extends JTextField			//The Text Field showing when looking at the vocabulary
	{
		int multWord = -1;
		
		public wordField(vocabLocation word, int lang, int multWord)
		{
			this.multWord = multWord;
			initiate(word,lang);
		}
		
		public wordField(vocabLocation word, int lang)
		{
			initiate(word,lang);
		}
		
		void initiate(vocabLocation word, int lang)
		{
			this.setText(languages.get(lang).getWord(word));
			this.setFont(new Font("Serif", Font.PLAIN, 20));
			this.setPreferredSize(new Dimension(200,40));
			this.addMouseListener(new MouseAdapter()		//Calls the word Menu when rightclicking on word
			{
				public void mousePressed(MouseEvent e)
				{
					if(e.getButton() == MouseEvent.BUTTON3)
						if(languages.get(lang).getLang().get(word.word()).isSingular())
							wordOptions(lang, word.word(),false,multWord);
						else
							multWordMenu(lang, word.word());
				}
			});
			this.addKeyListener(new KeyListener()	//Saves the data if word is changed
			{
				public void keyReleased(KeyEvent e) 
				{									
					String changedWord = getThisText();
					if(multWord<0)
						languages.get(lang).getLang().get(word.word()).setWord(changedWord, word.language());
					else
					{
						multWords mult = (multWords)languages.get(lang).getLang().get(word.word());
						mult.get(multWord).setWord(changedWord, word.language());
						languages.get(lang).getLang().set(multWord, mult);
					}
					writeFile(languagesPath, languages);
				}
				public void keyTyped(KeyEvent e) {}
				public void keyPressed(KeyEvent e) {}
			});
		}
		
		public String getThisText()
		{
			return this.getText();
		}
	}
	
	void multWordMenu(int lang, int word)
	{
		big.setCallable(new Callable<Void>() { 
			public Void call() {
				vocabMenu(lang);
				return null;
			}				//Implements where to go back to(what last menu was)
		});	
		big.getContentPane().removeAll();
		JPanel panel = new JPanel();
		JScrollPane sp = new JScrollPane(panel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		sp.getVerticalScrollBar().setUnitIncrement(16);		//Fast Scrollbar
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		multWords mult = (multWords)languages.get(lang).getLang().get(word);
		int size = mult.size();
		c.gridy = 0;					
		c.ipady = 40;
		JTextField title1 = new JTextField(mult.getWord((byte)0));	//Create it to focus it
		JTextField title2 = new JTextField(mult.getWord((byte)1));
		title1.setFont(new Font("Serif", Font.PLAIN, 20));
		title1.setPreferredSize(new Dimension(200,40));
		title1.setBackground(Color.black);
		title1.setForeground(Color.white);
		title2.setFont(new Font("Serif", Font.PLAIN, 20));
		title2.setPreferredSize(new Dimension(200,40));
		title2.setBackground(Color.black);
		title2.setForeground(Color.white);
		JPanel titlePanel1 = new JPanel(new GridBagLayout());
		titlePanel1.add(title1);
		panel.add(titlePanel1, c);
		c.gridx = 1;
		JPanel titlePanel2 = new JPanel(new GridBagLayout());
		titlePanel1.add(title2);
		panel.add(titlePanel2, c);
		c.gridx = 0;
		for(int i=0;i<size;i++)
		{
			c.gridy = 2*i+2;
			JTextField field1 = new wordField(new vocabLocation(word,(byte)0), lang, i);
			JPanel pane1 = new JPanel(new GridBagLayout());
			pane1.add(field1);
			c.gridx = 0;
			panel.add(pane1, c);
			JTextField field2 = new wordField(new vocabLocation(word,(byte)1), lang, i);
			JPanel pane2 = new JPanel(new GridBagLayout());
			pane2.add(field2);
			c.gridx = 1;
			panel.add(pane2, c);
			c.gridy = 2*i+3;
			c.ipady = 40;
		}
		JButton addNew = new JButton("+");
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		addNew.setFont(new Font("Serif", Font.PLAIN, 40));
		addNew.setFocusPainted(false);
		addNew.setBackground(Color.white);
		addNew.addActionListener(e -> addWord(lang, word));
		addNew.setPreferredSize(new Dimension(70,70));
		buttonPanel.add(addNew);
		c.gridy = 2*size+2;
		panel.add(buttonPanel, c);
		c.gridy = 2*size+3;
		c.ipady = 40;
		big.add(sp);
		title1.requestFocusInWindow();
		big.revalidate();
	}
	
	void wordOptions(int lang, int word, boolean newWord, int ifMultWord)
	{
		if(newWord)
		{
			JFrame frame = new smallFrame();
			JButton sing = new JButton("single Word"), mult = new JButton("multiple Words");
			JPanel panel = new JPanel();
			sing.addActionListener(e -> {frame.dispose(); wordOptionsSing(lang, word, newWord, ifMultWord);});
			mult.addActionListener(e -> {languages.get(lang).getLang().add(new multWords("","")); 
								writeFile(languagesPath, languages); frame.dispose(); multWordMenu(lang, word);});
			panel.add(sing); panel.add(mult);
			frame.add(panel);
		}
		else
		{
			if(ifMultWord<0)
				wordOptionsSing(lang, word, newWord, ifMultWord);
			else
				multWordMenu(lang, word);
		}
	}
	
	void wordOptionsSing(int lang, int word, boolean newWord, int ifMultWord)
	{
		JFrame frame = new smallFrame();
		frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.PAGE_AXIS));
		JPanel row0 = new JPanel(), row1 = new JPanel(), row2 = new JPanel(), row3 = new JPanel(), row4 = new JPanel(), row5 = new JPanel(), row6 = new JPanel(), row7 = new JPanel();
		JButton delete = new JButton("Delete"), save = new JButton("Save"), exit = new JButton("Exit");
		JLabel lang1 = new JLabel("Word 1:"), lang2 = new JLabel("Word 2:"), level1 = new JLabel("Level Word 1:"), 
				level2 = new JLabel("Level Word 2:"), time1 = new JLabel("Word 1 time:"), time2 = new JLabel("Word 2 time:");
		JTextField lang1Field = new JTextField(languages.get(lang).getLang().get(word).getWord((byte)0)),
					lang2Field = new JTextField(languages.get(lang).getLang().get(word).getWord((byte)1)),
					level1Field = new JTextField(languages.get(lang).getLang().get(word).getLevel((byte)0)+""),
					level2Field = new JTextField(languages.get(lang).getLang().get(word).getLevel((byte)1)+""),
					time1Field = new JTextField(languages.get(lang).getLang().get(word).getTime((byte)0)+""),
					time2Field = new JTextField(languages.get(lang).getLang().get(word).getTime((byte)1)+"");
		lang1Field.setPreferredSize(new Dimension(100,18));
		lang2Field.setPreferredSize(new Dimension(100,18));
		level1Field.setPreferredSize(new Dimension(80,18));
		level2Field.setPreferredSize(new Dimension(80,18));
		time1Field.setPreferredSize(new Dimension(100,18));
		time2Field.setPreferredSize(new Dimension(100,18));
		delete.setBackground(Color.red);
		delete.setFocusPainted(false);
		delete.addActionListener(e -> 
		{languages.get(lang).getLang().remove(word);frame.dispose();writeFile(languagesPath, languages);if(ifMultWord<0) vocabMenu(lang); else multWordMenu(lang, ifMultWord);});
		exit.addActionListener(newWord ? e -> {if(ifMultWord<0)languages.get(lang).getLang().remove(word); else { multWords words = (multWords)languages.get(lang).getLang().get(ifMultWord); words.remove(word);} frame.dispose();}: e -> frame.dispose());
		save.addActionListener(e ->
		{Word currentWord = new Word(lang1Field.getText(), lang2Field.getText(), Integer.parseInt(level1Field.getText()), 
				Integer.parseInt(level2Field.getText()), Long.parseLong(time1Field.getText()), Long.parseLong(time2Field.getText()));
		if(ifMultWord<0) languages.get(lang).getLang().set(word, currentWord); 
		else{multWords words = (multWords)languages.get(lang).getLang().get(ifMultWord); words.set(word, currentWord);}
		frame.dispose();if(ifMultWord<0) vocabMenu(lang); else multWordMenu(lang, ifMultWord);
		writeFile(languagesPath, languages);});
		if(!newWord)
			row0.add(delete);
		row1.add(lang1); row1.add(lang1Field);
		row2.add(lang2); row2.add(lang2Field);
		row3.add(level1); row3.add(level1Field);
		row4.add(level2); row4.add(level2Field);
		row5.add(time1); row5.add(time1Field);
		row6.add(time2); row6.add(time2Field);
		row7.add(save); row7.add(exit);
		frame.add(row0);
		frame.add(row1); frame.add(row2); frame.add(row3); 
		frame.add(row4); frame.add(row5); frame.add(row6); 
		frame.add(row7); 
		frame.repaint();
		frame.revalidate();
	}
	
	void addWord(int lang, int multWord)
	{
		languages.get(lang).getLang().add(new Word("", "", 0, 0, 0l, 0l));
		writeFile(languagesPath, languages);
		vocabMenu(lang);
	}
	
	//Reads files and returns object. Rights empty object of class c if there is no file on the given path.
	<T> Object readFile(String path, Class<T> c)
	{
		Object o = null;
		File file = new File(path);
		if(!file.exists())
		{
			try {
				file.createNewFile();
			} catch (IOException e1) {e1.printStackTrace();
			}
			try {
				writeFile(path, c.getConstructor().newInstance());
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e1) {
				e1.printStackTrace();}
		}
		try
		{
			FileInputStream fis = new FileInputStream(path);
			ObjectInputStream ois = new ObjectInputStream(fis);
			o = ois.readObject();
			ois.close();
		}
		catch(IOException | ClassNotFoundException e){e.printStackTrace();}
		return o;
	}
	
	//Writes an object to a file
	void writeFile(String path, Object o)
	{
		try
		{
			File file = new File(path);
			file.createNewFile();
			FileOutputStream fos = new FileOutputStream(path);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(o);
			oos.close();
		}
		catch(IOException e) {e.printStackTrace();}
	}

	public static void main(String[] args)
	{
		VocabularyTester tester = new VocabularyTester();
	}
	
	//--------------------------------------------------------setPath stuff???----------------------------------------------
}

//Creating the Object Language including vocabularies which can be either words or a collection of words like the conjugations of a verb.

interface wordInstance
{
	boolean testable(byte b);
	boolean isSingular();
	String getWord(byte b);
	void setWord(String word, byte b);
	int getLevel(byte b);
	long getTime(byte b);
}

class Word implements wordInstance, Serializable
{
	private static final long serialVersionUID = 1L;
	private String word1, word2;
	private int level1, level2;
	private long time1, time2;
	
	public Word()
	{
		word1 = "";
		word2 = "";
		level1 = 0;
		level2 = 0;
		time1 = 0;
		time2 = 0;
	}
	
	public Word(String word1, String word2, int level1, int level2, long time1, long time2)
	{
		this.word1 = word1;
		this.word2 = word2;
		this.level1 = level1;
		this.level2 = level2;
		this.time1 = time1;
		this.time2 = time2;
	}
	
	@Override
	public boolean testable(byte b)
	{
		return System.currentTimeMillis() >= (b==0 ? time1 : time2) 
				+ 1000*60*60*24*Math.pow(2, (b==0 ? level1 : level2) - 1);
	}

	@Override
	public String getWord(byte b) 
	{
		return b==0 ? word1 : word2;
	}

	@Override
	public int getLevel(byte b)
	{
		return (b==0 ? level1 : level2);
	}

	@Override
	public long getTime(byte b)
	{
		return (b==0 ? time1 : time2);
	}
	
	@Override
	public void setWord(String word, byte b)
	{
		if(b==0)
			word1 = word;
		else
			word2 = word;
	}
	
	void setLevel(int level, byte b)
	{
		if(b==0)
			level1 = level;
		else
			level2 = level;
	}
	
	void setTime(long time, byte b)
	{
		if(b==0)
			time1 = time;
		else
			time2 = time;
	}

	@Override
	public boolean isSingular() 
	{
		return true;
	}
}

class multWords extends ArrayList<Word> implements wordInstance, Serializable
{
	private static final long serialVersionUID = 1L;
	private String titleA, titleB;
	private boolean testTitle;
	
	public multWords(String titleA, String titleB, boolean testTitle)
	{
		this.titleA = titleA;
		this.titleB = titleB;
		this.testTitle = testTitle;
	}
	
	public multWords(String titleA, String titleB)
	{
		this.titleA = titleA;
		this.titleB = titleB;
		testTitle = false;
	}
	
	@Override
	public boolean testable(byte b)
	{
		return this.stream().anyMatch(word -> word.testable(b));
	}
	
	@Override
	public boolean isSingular() 
	{
		return false;
	}

	public ArrayList<String> getLangVocabs(byte b)	//Returns list of all words in chosen langugage
	{
		return new ArrayList<String>(this.stream().map(Word -> Word.getWord(b)).collect(Collectors.toList()));
	}

	@Override
	public String getWord(byte b) 
	{
		return b==0 ? titleA : titleB;
	}
	
	@Override
	public void setWord(String title, byte b)
	{
		if(b==0)
			titleA = title;
		else
			titleB = title;
	}
	
	public boolean titleTestable()
	{
		return testTitle;
	}

	@Override
	public int getLevel(byte b) {
		return 0;
	}
	@Override
	public long getTime(byte b) {
		return 0;
	}
}

class language implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String nameA, nameB, name;
	private ArrayList<wordInstance> lang = new ArrayList<wordInstance>();
	
	public language(String nameA, String nameB, String name, ArrayList<wordInstance> lang)
	{
		this.nameA = nameA;
		this.nameB = nameB;
		this.name = name;
		this.lang = lang;
	}
	
	public language(String nameA, String nameB, ArrayList<wordInstance> lang)
	{
		this.nameA = nameA;
		this.nameB = nameB;
		name = nameA + " - " + nameB;
		this.lang = lang;
	}
	
	public language(String nameA, String nameB, String name)
	{
		this.nameA = nameA;
		this.nameB = nameB;
		this.name = name;
	}
	
	public language(String nameA, String nameB)
	{
		this.nameA = nameA;
		this.nameB = nameB;
		name = nameA + " - " + nameB;
	}
	
	String getLangName(byte b)
	{
		return b == 0 ? nameA : nameB;
	}
	
	String getName()
	{
		return name;
	}
	
	ArrayList<wordInstance> getLang()
	{
		return lang;
	}
	
	String getWord(VocabularyTester.vocabLocation location)
	{
		return lang.get(location.word()).getWord(location.language());
	}
	
	void LangName(byte b, String name)
	{
		if(b==0)
			nameA = name;
		else if(b==1)
			nameB = name;
	}
	
	void setName(String name)
	{
		this.name = name;
	}
	
	void setLang(ArrayList<wordInstance> lang)
	{
		this.lang = lang;
	}
}